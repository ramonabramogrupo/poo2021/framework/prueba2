<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "competencias".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property int|null $idEstudio
 *
 * @property Estudios $idEstudio0
 */
class Competencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEstudio'], 'integer'],
            [['descripcion'], 'string', 'max' => 500],
            [['idEstudio'], 'exist', 'skipOnError' => true, 'targetClass' => Estudios::className(), 'targetAttribute' => ['idEstudio' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'idEstudio' => 'Id Estudio',
        ];
    }

    /**
     * Gets query for [[IdEstudio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEstudio0()
    {
        return $this->hasOne(Estudios::className(), ['id' => 'idEstudio']);
    }
}
