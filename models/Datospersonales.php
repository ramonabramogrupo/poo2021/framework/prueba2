<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "datospersonales".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $telefono
 * @property string|null $email
 * @property string|null $linkedin
 * @property string|null $carnetConducir
 * @property string|null $calle
 * @property string|null $numero
 * @property string|null $provincia
 * @property string|null $poblacion
 * @property string|null $cp
 * @property string|null $foto
 */
class Datospersonales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'datospersonales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'email', 'linkedin', 'provincia', 'poblacion', 'foto'], 'string', 'max' => 100],
            [['apellidos', 'calle'], 'string', 'max' => 255],
            [['telefono'], 'string', 'max' => 20],
            [['carnetConducir'], 'string', 'max' => 50],
            [['numero'], 'string', 'max' => 10],
            [['cp'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'linkedin' => 'Linkedin',
            'carnetConducir' => 'Carnet Conducir',
            'calle' => 'Calle',
            'numero' => 'Numero',
            'provincia' => 'Provincia',
            'poblacion' => 'Poblacion',
            'cp' => 'Cp',
            'foto' => 'Foto',
        ];
    }
    
    public function getNombreCompleto(){
        return "$this->nombre $this->apellidos";
    }
    
    public function getDireccion(){
        return "$this->calle, $this->numero";
    }
    
    public function getPoblacion(){
        return "$this->cp, $this->poblacion";
    }
}
