<?php
use yii\widgets\ListView;
?>
<h2 class="bg-dark text-white p-2 my-3">
    <?= $titulo ?>
</h2>
<?=
    ListView::widget([
        "dataProvider" => $dataProvider,
        "itemView" => "_academica",
        "layout" =>"{items}",
        "options" => ["class"=>"caja p-2"],
        "itemOptions" => ["class"=>"elemento p-2 mb-3"]
    ]); 
?>



