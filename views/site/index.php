<?php
use app\widgets\Card;
use yii\helpers\Html;

/*echo \yii\widgets\DetailView::widget([
    "model" => $model,
    "attributes" =>[
        "nombre",
        "apellidos",
        "direccion"
            
    ],
    "template" => function ($atributo){
        return "<div>" . $atributo['label'] . "</div><div>" . $atributo['value'] . "</div>";
    }
]); */


/*foreach ($model as $label=>$campo){
?>
<div> <?= $label ?></div>
<div><?= $campo ?></div>
<?php
}*/

// a mano
?>
<div class="row">
<div class="col-lg-5">
<?= Card::widget([
    "titulo" => "Foto Perfil",
    "contenido" => Html::img("@web/imgs/$model->foto",["class"=>"img-fluid"])
]);?>

<?= Card::widget([
    "titulo" => "Nombre completo",
    "contenido" => $model->getNombreCompleto()
]);?>
</div>
<div class="col-lg-7">
<?= Card::widget([
    "titulo" => "Direccion",
    "contenido" => $model->getDireccion() . "<br>" . $model->getPoblacion() . "<br>" . $model->provincia
]);?>

<?= Card::widget([
    "titulo" => "Correo Electronico",
    "contenido" => $model->email
]);?>

<?= Card::widget([
    "titulo" => "Red Linkedin",
    "contenido" => $model->linkedin
]);?>

<?= Card::widget([
    "titulo" => "Telefono",
    "contenido" => $model->telefono
]);?>

<?= Card::widget([
    "titulo" => "Carnets de Conducir",
    "contenido" => $model->carnetConducir
]);?>
</div>
</div>



