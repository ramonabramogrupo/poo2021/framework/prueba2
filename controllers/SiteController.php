<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Datospersonales;
use yii\data\ActiveDataProvider;
use app\models\Estudios;
use app\models\Experiencia;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // consulta sin ejecutar
        //$consulta= Datospersonales::find();
        //$modelo=$consulta->one();

        // consulta ejecutada y lo que me duelve es un modelo con los datos
        $modelo=Datospersonales::find()->one();
        
        // consulta ejecutada y lo que me devuelve es un array de modelos
        //$modelos=Datospersonales::find()->all();
        //$modelo=$modelos[0];
        
        return $this->render('index',[
            "model" => $modelo
        ]);
    }
    
    public function actionAcademica(){
        
        // consulta de los estudios realizados
        $consulta=Estudios::find()->where([
            "complementarios" => 0
        ]);
        
        // Data Provider con el resultado de la consulta anterior 
        // para mandarlo a un listView
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("academica",[
            "dataProvider" => $dataProvider,
            "titulo" => "Formacion Academica"
        ]);
        
    }
    
    public function actionComplementaria(){
        
        // consulta de los estudios realizados
        $consulta=Estudios::find()->where([
            "complementarios" => 1
        ]);
        
        // Data Provider con el resultado de la consulta anterior 
        // para mandarlo a un listView
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("academica",[
            "dataProvider" => $dataProvider,
             "titulo" => "Formacion Complementaria"
        ]);
        
    }
    
    public function actionExperiencia(){
        
        // consulta de la experiencia
        $consulta= Experiencia::find();
        
        // Data Provider con el resultado de la consulta anterior 
        // para mandarlo a un listView
        $dataProvider=new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("experiencia",[
            "dataProvider" => $dataProvider,
             "titulo" => "Experiencia Laboral"
        ]);
        
    }
    
}
