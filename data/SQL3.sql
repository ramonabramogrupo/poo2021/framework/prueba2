﻿DROP DATABASE IF EXISTS prueba2;
CREATE DATABASE prueba2;
USE prueba2;

CREATE TABLE datosPersonales(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(255),
  telefono varchar(20),
  email varchar(100),
  linkedin varchar(100),
  carnetConducir varchar(50),
  calle varchar(255),
  numero varchar(10),
  provincia varchar(100),
  poblacion varchar(100),
  cp varchar(5),
  foto varchar(100),
  PRIMARY KEY(id)

  );

CREATE TABLE estudios(
  id int AUTO_INCREMENT,
  titulo varchar(255),
  fechas varchar(255),
  centro varchar(255),
  complementarios boolean DEFAULT FALSE,
  PRIMARY KEY(id)
  );

CREATE TABLE competencias(
  id int AUTO_INCREMENT,
  descripcion varchar(500),
  idEstudio int,
  PRIMARY KEY(id)
  );

CREATE TABLE experiencia (
  id int AUTO_INCREMENT,
  empresa varchar(255),
  poblacion varchar(255),
  puesto varchar(255),
  fechas varchar(255),
  PRIMARY KEY(id)
  );

CREATE TABLE funciones(
  id int AUTO_INCREMENT,
  descripcion varchar(800),
  idExperiencia int,
  PRIMARY KEY(id)
  );


CREATE TABLE otros(
  id int AUTO_INCREMENT,
  nombre varchar(255),
  nivel enum('A1','A2','B1','B2','C1','C2'),
  tipo varchar(255),
  PRIMARY KEY(id)
  );

ALTER TABLE competencias
  ADD CONSTRAINT fkCompetenciasEstudios FOREIGN KEY(idEstudio) REFERENCES estudios(id)
  ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE funciones 
  ADD CONSTRAINT fkFuncionesExperiencia FOREIGN KEY(idExperiencia)
  REFERENCES experiencia(id) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO datosPersonales (nombre, apellidos, telefono, email, linkedin, carnetConducir, calle, numero, provincia, poblacion, cp, foto)
  VALUES ('Ramon', 'Abramo Feijoo', '629 629 629', 'ramonabramo@gmail.com', 'linkedin.com/ramonabramo', 'A1, A2, B1', 'Pasaje Peña', '1', 'Cantabria', 'Santander', '39008', 'ramon.png');

INSERT INTO estudios (id,titulo, fechas, centro)
  VALUES (1,'ingenieria telecomunicaciones', '1996-1991', 'Universidad Cantabria'),
         (2,'ciclo formativo grado superior', '1989-1991','Instituto politecnico');

INSERT INTO competencias (descripcion, idEstudio) VALUES 
  ('Trabajar con circuitos electronicos ',1),
  ('Programar en lenguajes orientados a objetos',1),
  ('Automatas programables',2),
  ('Programacion en ensamblador',2);

INSERT INTO estudios (id,titulo, fechas, centro, complementarios) VALUES 
  (3,'Programacion orientada a objetos','06/2021-12/2021','Alpe Formación',TRUE),
  (4,'Bases de datos relacionales','01/2020-06/2020','Alpe Formación',TRUE);

INSERT INTO competencias (descripcion, idEstudio) VALUES 
  ('Trabajar con circuitos electronicos ',3),
  ('Programar en lenguajes orientados a objetos',3),
  ('Automatas programables',4),
  ('Programacion en ensamblador',4);

INSERT INTO experiencia (id, empresa, poblacion, puesto, fechas) VALUES 
(1, 'Alpe Formacion', 'Santander', 'Docente', '2000-2001'),
(2, 'Alpe Formacion', 'Santander', 'Docente', '2001-2002'),
(3, 'Alpe Formacion', 'Santander', 'Docente', '2003-2004');

INSERT INTO funciones (descripcion, idExperiencia) VALUES 
('Impartir clase ciclos', 1),
('Impartir clase certificados', 1),
('Impartir clase ciclos', 2),
('Impartir clase certificados', 2),
('Impartir clase ciclos', 3),
('Impartir clase certificados', 3),
('Impartir curso privado', 1),
('Impartir formacion a distancia programacion web', 1);



